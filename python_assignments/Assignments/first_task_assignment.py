import re
s="GameOfThrones"
#extract from given input =>'one'
print(s[9:12])
#extract from first input one=>'One'
s1=s[9:12]
print(s1.capitalize())
#reverse the given input
print(s[::-1])
#reverse the inputcase
print(s.swapcase())
#extract caps into one string==>'GOT'
print(''.join(re.findall('[A-Z]+',s)))
#convert int to float
i=float(20)
print(i)
print(type(i))
#convert higher datatype to lower datatype
k=120
j=int("234")
#if we add directly it throw type error, do type conversion first
print(k+j)