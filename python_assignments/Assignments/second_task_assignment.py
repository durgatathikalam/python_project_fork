from typing import Any

import datetime

class Organization:
    def __init__(self,companyname):
        self.name=companyname #instance variables

    def get_name(self):
        return self.name
    #to print and return the name object use of str func
    def __str__(self):
        return "Organization:"+str(self.get_name())


class Company(Organization):

    def __init__(self,companyname):
        Organization.__init__(self,companyname)
        self.no_of_employees=0
    
    def get_no_of_employees(self):
        return self.no_of_employees
    def set_no_of_employees(self,value):
        self.no_of_employees=value
    
    def __str__(self):
        return "Company: "+str(self.get_name)+" Number of employees:"+str(self.get_no_of_employees())

class Employee:

    def __init__(self,name,gender,jobtitle,company,dob):
        '''these are all instance variables'''
        self.name=name
        self.gender=gender
        self.jobtitle=jobtitle
        self.date_of_birth(dob)
        self.company=company
        employee_count=self.company.get_no_of_employees()
        company.set_no_of_employees(employee_count+1)
       
    def date_of_birth(self,dob):
        self.birthday=datetime.datetime.strptime(dob,"%d/%m/%Y")
    def get_name(self):
        return self.name
    
    def get_gender(self):
        return self.gender
    
    def get_jobtitle(self):
        return self.jobtitle
    
    def get_dob(self):
        return self.birthday

    def __eq__(self,other):
        check_name=self.name==other.name
        check_gender=self.gender==other.gender
        check_dob=self.birthday==other.birthday
        check_company=self.company.name==other.company.name
        return check_name and check_gender and check_dob and check_company
    
    def __str__(self):
        return "Employee:"+str(self.get_name())+" Gender:"+str(self.get_gender())+" Job Title:"+str(self.get_jobtitle())+" DOB:"+str(self.get_dob().strftime("%d/%m/%Y"))+" Company:"+str(self.company.get_name())
    
try:
    if __name__ == "__main__":
         Google=Company("Google")
         Microsoft=Company("Microsoft")
         emp1=Employee("disha","male","Python developer",Google,"30/04/1995")
         emp2=Employee("disha","male","Python developer",Google,"30/04/1995")
         emp3=Employee("Mrathon","male","Python developer",Google,"30/04/1995")
         emp4=Employee("Mike","male","Python developer",Microsoft,"30/04/1995")
         print(emp1)
         print(emp1==emp2)
         print(emp1==emp3)
         print(emp3.company.get_no_of_employees())
         print(Google.get_no_of_employees())
         print(Microsoft.get_no_of_employees())
finally:
    print("comparisions are done")
